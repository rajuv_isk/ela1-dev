<?php
function brazosilo_theme_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );

        wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( 'parent-style' ),
        wp_get_theme()->get('Version')
    );
    wp_enqueue_style( 'divichild-aosstyle', get_stylesheet_directory_uri().'/css/aos.css' );
	wp_enqueue_script( 'divichild-aosjs', 'https://unpkg.com/aos@2.3.1/dist/aos.js');
	
    wp_enqueue_script( 'customjs', get_stylesheet_directory_uri().'/js/custom.js', array('jquery'), false, true )  ;  


}
add_action( 'wp_enqueue_scripts', 'brazosilo_theme_enqueue_styles' );



add_filter('use_block_editor_for_post', '__return_false', 10);
add_filter('use_block_editor_for_post_type', '__return_false', 10);

/*Svg file uploads*/
/*function svg_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'svg_mime_types');*/

function add_file_types_to_uploads($file_types){
    $new_filetypes = array();
    $new_filetypes['svg'] = 'image/svg+xml';
    $file_types = array_merge($file_types, $new_filetypes );
    return $file_types;
}
add_filter('upload_mimes', 'add_file_types_to_uploads');