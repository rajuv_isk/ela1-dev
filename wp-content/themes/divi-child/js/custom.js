jQuery(document).ready(function(){

	  jQuery('#custom-audioplay').click(function(e) {
	  	var video =jQuery('#custom-audioplay video')[0];
		  video.style.display = "none";
		  if (video.currentTime < 1 || video.paused) {
		    video.play();
		  } else if (video.ended) {
		    video.pause();
		    video.currentTime = 0;
		    video.play();
		  } else {
		    video.pause();
		  }
		});

});